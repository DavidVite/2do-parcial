class Abuela
{
    constructor()
    {
        this.sangre = "O+";
        this.piel = "Morena";
        this.casado = "No";
        this.colorojos = "Cafe";
        this.estatura = "1.48";

        //Abuela Paterna
        this.nombre = "Herminia";
        this.AaP_apelpat = "Vite";
        this.AaP_apelmat = "Aquino";
        console.log(this.nombre + " " + this.AaP_apelpat + " " + this.AaP_apelmat);

        //Abuela Materna
        this.nombre = "Marcelina"
        this.AaM_apelpat = "Velasco"
        this.AaM_apelmat = "Bautista"
        console.log(this.nombre + " " + this.AaM_apelpat + " " + this.AaM_apelmat);

        //Abuelo Materno
        this.nombre = "Francisco"
        this.AoM_apelpat = "Bautista"
        this.AoM_apelmat = "Domingo"
        console.log(this.nombre + " " + this.AoM_apelpat + " " + this.AoM_apelmat);

    }
}
