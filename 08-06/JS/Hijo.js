class Hijo extends Padre
{
    constructor()
    {
        super();
        //Yo
        this.nombre = " Jose David";
        this.estatura = "1.70";
        console.log(this.nombre + " " + this.AaP_apelpat + " " + this.AoM_apelpat);

        //Hermano 1
        this.nombre = " Rodrigo";
        this.estatura = "1.66";
        console.log(this.nombre + " " + this.AaP_apelpat + " " + this.AoM_apelpat);

        //Hermano 2
        this.nombre = "Cristian Geovany";
        this.estatura = "1.52";
        console.log(this.nombre + " " + this.AaP_apelpat + " " + this.AoM_apelpat);

        //Hermano 3
        this.nombre = " Diego";
        this.estatura = "0.80";
        console.log(this.nombre + " " + this.AaP_apelpat + " " + this.AoM_apelpat);

    }
}

