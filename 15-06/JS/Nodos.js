class Nodos
{
    constructor(Nivel, nodoPadre, hI=0, hD=0)
    {
        this.nivel = Nivel;
        this.posicion = Posicion;
        this.nodopadre = nodoPadre;
        this.izquierdo = hI;
        this.derecho = hD;

        //Crear Hijo izquierdo
        if(hI == 1){
            this.hijoizquierdo = this.crearHijo("izq",0+1, this.nivel);
        }

        //Crear Hijo Derecho
        if (hD == 1) {
            this.hijoderecho = this.crearHijo("der",0+1, this.nivel);
        }
    }
    crearHijo (Posicion, Nivel)
    {
        var crearhijo = new Nodo(Posicion, Nivel, nodoPadre);
        return crearhijo;
    }
}