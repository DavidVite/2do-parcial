class Nodo2
{
    constructor(Posicion, Nivel, HijoI=0 , HijoD=0, PadreD=0, PadreI = 0)
    {
        this.posicion = Posicion;
        this.nivel = Nivel;
        this.hijoizquierdo = HijoI;
        this.hijoderecho = HijoD;
        this.padred = PadreD;
        this.padrei = PadreI

        //Crear Hijo izquierdo
        if(HijoI == 1){
            this.hijoizquierdo = this.crearHijo("izq",0 + 1, this.nivel);
        }

        //Crear Hijo Derecho
        if (HijoD == 1) {
            this.hijoderecho = this.crearHijo("der",0 + 1,this.nivel);
        }

        //Crear Padre Derecho
        if (PadreD == 1)
        {
            this.PadreDer = this.crearPadre("der", 0+2, 1,1, 0, 0, this.nivel);
        }

        //Crear Padre Izquierdo
        if (PadreI == 1)
        {
            this.PadreIzq = this.crearPadre("izq", 0+1, 1,0,1,0, this.nivel);
        }
    }

    crearHijo (Posicion, Nivel)
    {
        var crearhijo = new Nodo2(Posicion, Nivel);
        return crearhijo;
    }
    crearPadre(Posicion, Nivel, HijoI, HijoD, PadreD, PadreI)
    {
        var crearPadre = new Nodo2(Posicion, Nivel, HijoI, HijoD, PadreD,PadreI);
        return crearPadre;
    }
}
