class Arbol2
{
    constructor()
    {
        this.nodoPadre = this.agregarNodoPadre();
    }

    agregarNodo(Tipo, Nivel, HijoI=0, HijoD=0) {
        var nodo = new Nodo2(Tipo, Nivel, HijoI, HijoD);
        return nodo;
    }

    agregarNodoPadre() {
        var nodo = new Nodo2("Cen", 0, 0, 1, 0, 1);
        return nodo;
    }
}