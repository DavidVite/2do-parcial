class Nodo
{
    constructor(Tipo, Valor, Nivel, Padre, HijoI=0 , HijoD=0)
    {
        this.tipo = Tipo;
        this.valor = Valor;
        this.nivel = Nivel;
        this.izquierdo = HijoI;
        this.derecho = HijoD;

        //Crear Hijo izquierdo
        if(HijoI == 1){
            this.hijoizquierdo = this.crearHijo("izq", 0, 0 + 1, this.nivel);
        }


        //Crear Hijo Derecho
        if (HijoD == 1) {
            this.hijoderecho = this.crearHijo("der", 0, 0 + 1, this.nivel);
        }

    }

    crearHijo (Tipo, Valor, Nivel, Padre)
    {
        var crearhijo = new Nodo(Tipo, Valor, Nivel, Padre);
        return crearhijo;
    }
}