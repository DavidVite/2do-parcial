class Arbol
{
    constructor() {
        this.nodoPadre = this.agregarNodoPadre();
    }

    agregarNodo(Tipo, Valor, Nivel, Padre, HijoI=0, HijoD=0) {
        var nodo = new Nodo(Tipo, Valor, Nivel, Padre, HijoI, HijoD);
        return nodo;
    }

    agregarNodoPadre() {
        var nodo = new Nodo("izq", true, 0, 0, 1, 1);
        return nodo;
    }
}