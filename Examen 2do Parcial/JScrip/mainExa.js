var arboles = new ArbolExa();
var buscar = 2;


//Nivel 1
arboles.nodoPadre.hI = arboles.agregarNodo(arboles.nodoPadre,"Lado Izquierdo","16", 16);
arboles.nodoPadre.hD = arboles.agregarNodo(arboles.nodoPadre,"Lado Derecho","20",20);

//Nivel 2
arboles.nodoPadre.hI.hI = arboles.agregarNodo(arboles.nodoPadre.hI,"Lado Izquierdo","8",8);
arboles.nodoPadre.hI.hD = arboles.agregarNodo(arboles.nodoPadre.hD,"Lado Derecho","8",8);

arboles.nodoPadre.hD.hI = arboles.agregarNodo(arboles.nodoPadre.hD,"Lado Izquierdo","8",8);
arboles.nodoPadre.hD.hD = arboles.agregarNodo(arboles.nodoPadre.hI,"Lado Derecho","12",12);

//Nivel 3
arboles.nodoPadre.hI.hD.hI = arboles.agregarNodo(arboles.nodoPadre.hD.hI,"Lado Izquierdo","a",4);
arboles.nodoPadre.hI.hD.hD = arboles.agregarNodo(arboles.nodoPadre.hD.hD,"Lado Derecho","4",4);

arboles.nodoPadre.hI.hI.hI = arboles.agregarNodo(arboles.nodoPadre.hI.hI,"Lado Izquierdo","e",4);
arboles.nodoPadre.hI.hI.hD = arboles.agregarNodo(arboles.nodoPadre.hI.hD,"Lado Derecho","4",4);

arboles.nodoPadre.hD.hD.hI = arboles.agregarNodo(arboles.nodoPadre.hD.hI,"Lado Izquierdo","5",5);
arboles.nodoPadre.hD.hD.hD = arboles.agregarNodo(arboles.nodoPadre.hD.hD,"Lado Derecho"," ",7);

arboles.nodoPadre.hD.hI.hI = arboles.agregarNodo(arboles.nodoPadre.hI.hI,"Lado Izquierdo","4",4);
arboles.nodoPadre.hD.hI.hD = arboles.agregarNodo(arboles.nodoPadre.hI.hD,"Lado Derecho","4",4);

//Nivel 4
arboles.nodoPadre.hD.hI.hI.hI = arboles.agregarNodo(arboles.nodoPadre.hI.hI.hI,"Lado Izquierdo","i",2);
arboles.nodoPadre.hD.hI.hI.hD = arboles.agregarNodo(arboles.nodoPadre.hI.hI.hD,"Lado Derecho","2",2);

arboles.nodoPadre.hD.hI.hD.hI = arboles.agregarNodo(arboles.nodoPadre.hI.hD.hI,"Lado Izquierdo","h",2);
arboles.nodoPadre.hD.hI.hD.hD = arboles.agregarNodo(arboles.nodoPadre.hI.hD.hD,"Lado Derecho","s",2);

arboles.nodoPadre.hD.hD.hI.hD = arboles.agregarNodo(arboles.nodoPadre.hD.hI.hD,"Lado Derecho","f",3);
arboles.nodoPadre.hD.hD.hI.hI = arboles.agregarNodo(arboles.nodoPadre.hD.hI.hI,"Lado Izquierdo","2",2);

arboles.nodoPadre.hI.hI.hD.hD = arboles.agregarNodo(arboles.nodoPadre.hI.hD.hD,"Lado Derecho","2",2);
arboles.nodoPadre.hI.hI.hD.hI = arboles.agregarNodo(arboles.nodoPadre.hI.hD.hI,"Lado Izquierdo","n",2);

arboles.nodoPadre.hI.hD.hD.hD = arboles.agregarNodo(arboles.nodoPadre.hD.hD.hD,"Lado Derecho","m",2);
arboles.nodoPadre.hI.hD.hD.hI = arboles.agregarNodo(arboles.nodoPadre.hD.hD.hI,"Lado Izquierdo","t",2);

//Nivel 5
arboles.nodoPadre.hI.hI.hD.hD.hI = arboles.agregarNodo(arboles.nodoPadre.hI.hD.hD.hI,"Lado Izquierdo","o",1);
arboles.nodoPadre.hI.hI.hD.hD.hD = arboles.agregarNodo(arboles.nodoPadre.hI.hD.hD.hD,"Lado Derecho","u",1);

arboles.nodoPadre.hD.hI.hI.hD.hI = arboles.agregarNodo(arboles.nodoPadre.hI.hI.hD.hI,"Lado Izquierdo","x",1);
arboles.nodoPadre.hD.hI.hI.hD.hD = arboles.agregarNodo(arboles.nodoPadre.hI.hI.hD.hD,"Lado Derecho","p",1);

arboles.nodoPadre.hD.hD.hI.hI.hI = arboles.agregarNodo(arboles.nodoPadre.hD.hI.hI.hI,"Lado Izquierdo","r",1);
arboles.nodoPadre.hD.hD.hI.hI.hD = arboles.agregarNodo(arboles.nodoPadre.hD.hI.hI.hD,"Lado Derecho","l",1);

//Imprime Arbol
console.log(arboles);

var resultado = arboles.verificarNivelHijos(arboles.nodoPadre);

//Info Del Nodo Buscado
var busqueda= arboles.buscarValor(buscar, arboles.nodoPadre);
//console.log(busqueda);

//Camino Del Nodo
var caminoNodo = arboles.buscarCaminoNodo(busqueda);
//console.log(caminoNodo);

//Suma De Camino
var sumaTotalCamino = arboles.sumarCaminoNodo(busqueda);
//console.log(sumaTotalCamino);
