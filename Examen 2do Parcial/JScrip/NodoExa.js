class NodoExa
{
    constructor(nodoPadre = null, posicion = null, nombre, Valor)
    {
        this.nombre = nombre;
        this.padre = nodoPadre;
        this.posicion = posicion;
        this.valor = Valor;
        if(nodoPadre == null)
        {
            this.nivel = 0;
        }
        else
        {
            this.nivel = nodoPadre.nivel + 1;
        }

    }
}