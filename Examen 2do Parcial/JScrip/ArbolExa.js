class ArbolExa
{
    constructor()
    {
        this.nodoPadre = this.agregarNodoPadre();
        this.nivel = 4;
        this.busquedaElementos;
        this.buscarNodos = [];
        this.caminoNodo = '';
        this.sumaCaminos = 0;
        this.yo = 'JDVB';
    }

    agregarNodoPadre()
    {
        var nodo = new NodoExa(null,null,"36", 36);
        return nodo;
    }

    agregarNodo(nodoPadre,posicion,nombre, Valor)
    {
        var nodo = new NodoExa(nodoPadre,posicion,nombre, Valor);
        return nodo;
    }

    verificarNivelHijos(nodo)
    {
        if(nodo.nivel == this.nivel)
            this.buscarNodos.push(nodo.nombre);

        if(nodo.hasOwnProperty('hI'))
            this.verificarNivelHijos(nodo.hI);

        if(nodo.hasOwnProperty('hD'))
            this.verificarNivelHijos(nodo.hD);

        return this.buscarNodos;
    }

    buscarValor(buscarElemento, nodo)
    {
        if(nodo.valor == buscarElemento)
            this.busquedaElementos = nodo;

        if(nodo.hasOwnProperty('hI'))
            this.buscarValor(buscarElemento, nodo.hI);

        if(nodo.hasOwnProperty('hD'))
            this.buscarValor(buscarElemento, nodo.hD);

        return this.busquedaElementos;
    }

    buscarCaminoNodo(nodo)
    {
        if(nodo.padre != null)
        {
            this.caminoNodo = this.caminoNodo+' '+nodo.padre.nombre;
            this.buscarCaminoNodo(nodo.padre)
        }
        return this.busquedaElementos.nombre+'  '+this.caminoNodo;
    }

    sumarCaminoNodo(nodo){
        if(nodo.padre != null)
        {
            this.sumaCaminos = this.sumaCaminos+nodo.padre.valor;
            this.sumarCaminoNodo(nodo.padre);
        }
        return this.busquedaElementos.valor+'  '+this.sumaCaminos;
    }
}